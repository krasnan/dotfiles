# Install Homebrew

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
brew update
brew upgrade

# Install packages
brew install htop
brew install iftop
brew install openssl
brew install tig
brew install composer
brew install php@7.4
brew install php@8.0
brew install php@8.1
brew install php@8.2
brew install git
brew install git-flow
brew install subversion
brew install python3
brew install thefuck
brew install wget
brew install zsh
brew install zsh-completions
brew install webkit2png
brew install unrar
brew install mysql
brew install imagemagick
brew install wifi-password
brew install nmap
brew install potrace
brew install aircrack-ng
brew install sqlmap

# Wait a bit before moving on...
sleep 1

# ...and then.
echo "Success! Basic brew packages are installed."


#productivity
brew install --cask alfred
brew install --cask notable
brew install --cask keepassxc # password manager
brew install --cask hiddenbar # hiddable items in status bar
brew install --cask numi # best calculator
brew install --cask alt-tab # app switcher
brew install --cask google-drive 
brew install --cask microsoft-office
# communication
brew install --cask slack
brew install --cask skype
brew install --cask whatsapp
brew install --cask teamviewer
# basics
brew install --cask stats # system info in status bar
brew install --cask spotify
brew install --cask the-unarchiver
brew install --cask appcleaner
brew install --cask inkscape
brew install --cask foxitreader
brew install --cask wine-stable
brew install --cask disk-inventory-x # analyza velkosti suborov na disku
brew install --cask hpedrorodrigues/tools/dockutil # cli pre editovanie poloziek v docku
# web
brew install --cask brave-browser
brew install --cask google-chrome
brew install --cask firefox
# dev
brew install --cask phpstorm
brew install --cask visual-studio-code
brew install --cask dbeaver-community
brew install --cask postman
brew install --cask dbngin # database installer and manager
brew install --cask openvpn-connect # vpn manager


# Quick Look Plugins (https://github.com/sindresorhus/quick-look-plugins)
brew install --cask qlcolorcode qlstephen qlmarkdown quicklook-json qlprettypatch quicklook-csv betterzipql qlimagesize webpquicklook qlvideo

# Wait a bit before moving on...
sleep 1

# ...and then.
echo "Success! Brew additional applications are installed."