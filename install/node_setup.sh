export NVM_DIR="$HOME/.nvm" && (
  git clone https://github.com/nvm-sh/nvm.git "$NVM_DIR"
  cd "$NVM_DIR"
  git checkout `git describe --abbrev=0 --tags --match "v[0-9]*" $(git rev-list --tags --max-count=1)`
) && \. "$NVM_DIR/nvm.sh"

nvm install 6.12 --latest-npm
nvm install 12 --latest-npm
nvm install --lts --latest-npm

npm install --global np
npm install --global git-open

brew install yarn --without-node